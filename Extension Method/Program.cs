﻿using System;

namespace ExtensionMethod
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var post = "This is supposed to be a very long blog post.";
            var shortenedPost = post.Shorten(5);
            Console.WriteLine(shortenedPost);

            Console.WriteLine("Main process has been completed!");
        }
    }
}
