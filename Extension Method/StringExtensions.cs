﻿using System.Linq;

namespace System
{
    public static class StringExtensions
    {
        public static string Shorten(this string str, int length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException(
                    $"Number of word ({length}) should be greater than or equal to 0.");
            if (length == 0) return "";
            var words = str.Split(' ');

            return words.Length <= length ? str : string.Join(' ', words.Take(length)) + " ...";
        }
    }
}