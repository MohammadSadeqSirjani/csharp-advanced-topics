﻿using System;
using System.Linq;

namespace LanguageIntegratedQuery
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var books = new BookRepository().GetBooks().ToList();

            //LINQ Query Operators
            //var cheapBooks = 
            //    from book in books
            //    where book.Price < 10
            //    orderby book.Title
            //    select book.Title;
            //foreach (var book in cheapBooks)
            //    Console.WriteLine($"{book}");

            //Console.WriteLine("***********");

            //LINQ Extension Methods
            //var cheapBooks = books.AsParallel().AsOrdered().Where(book => book.Price < 10).Select(book => book.Title);
            //foreach (var book in cheapBooks)
            //    Console.WriteLine($"Cheap book: {book}");

            //var noteBook = books.SingleOrDefault(note => note.Title == "ASP .Net MVC");
            //Console.WriteLine(noteBook?.Title);

            //var pagedBooks = books.Skip(2).Take(3);
            //foreach (var pagedBook in pagedBooks)
            //{
            //    Console.WriteLine($"{pagedBook.Title} {pagedBook.Price}");
            //}

            //var count = books.Count();
            //Console.WriteLine(count);

            //var (price, title) = books.Max(book => (book.Price,book.Title));
            //Console.WriteLine($"{title} {price}");

            //var (price, title) = books.Min(book => (book.Price, book.Title));
            //Console.WriteLine($"{title} {price}");

            var totalPrice = books.Sum(book => book.Price);
            Console.WriteLine($"{totalPrice}");
        }
    }
}
