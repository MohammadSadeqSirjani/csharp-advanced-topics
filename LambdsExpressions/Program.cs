﻿using System;
using System.Threading.Channels;

namespace LambdaExpressions
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            //args => expressions
            //var power = new Func<int, int>(number => number * number);
            //Console.WriteLine(power(5));

            //const int factor = 5;
            //var multiplier = new Func<int, int>(n => n * factor);
            //Console.WriteLine(multiplier(5));
            //Console.WriteLine(Square(5));

            var books = new BookRepository().GetBooks();

            var cheapBooks = books.FindAll(book => book.Price < 10);
            foreach (var book in cheapBooks)
            {
                Console.WriteLine(book.Title);
            }
        }

        /*
        private static bool IsCheaperThan10Dollars(Book book)
        {
            return book.Price < 10;
        }
        */
        /*
                private static int Square(int number)
                {
                    return number * number;
                }
        */
    }
}
