﻿namespace Generic.Model
{
    public class Book : Product
    {
        public int Isbn { get; set; }

        public Book()
        {

        }

        public Book(int isbn)
        {
            Isbn = isbn;
        }
    }
}
