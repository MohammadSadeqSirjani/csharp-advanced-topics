﻿namespace Generic.Model
{
    public abstract class Product
    {
        public float Price { get; set; }
        public string Title { get; set; }
    }
}
