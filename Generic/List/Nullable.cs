﻿namespace Generic.List
{
    public class Nullable<T> where T : struct
    {
        private readonly object _value;

        public Nullable()
        {
            
        }

        public Nullable(T value)
        {
            _value = value;
        }

        public bool HasValue => _value != null;

        public T GetValueOrDefault()
        {
            return HasValue ? (T) _value : default(T);
        }
    }
}