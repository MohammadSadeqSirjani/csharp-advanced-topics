﻿using System;

namespace Generic
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            //var numbers = new GenericLists<int>();
            //numbers.Add(10);

            //var books = new GenericLists<Book>();
            //books.Add(new Book());

            //var dictionary = new GenericDictionary<string, Book>();
            //dictionary.Add("Hello", new Book());

            var number = new List.Nullable<int>();
            Console.WriteLine($"Has value ? {number.HasValue}");
            Console.WriteLine($"Value: {number.GetValueOrDefault()}");


            Console.WriteLine("Main process has been completed!");
        }
    }
}
