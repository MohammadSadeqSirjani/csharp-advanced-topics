﻿using System;
using System.Threading;

namespace EventsAndDelegates
{
    public class VideoEncoder
    {
        // 1. Define a delegate
        // 2. Define  an event based on that delegates
        // 3. Raise the event

        #region Delegates
        //delegate

        // custom delegate for events
        // public delegate void VideoEncoderEventHandler(object source, VideoEventArgs args);

        public delegate void VideoFilter(Video video);
        #endregion

        #region Events
        //event
        
        // event handler using custom delegate
        // public event VideoEncoderEventHandler VideoEncoded;

        // Built-in delegate using for events 
        // EventHandler
        // EventHandler<TEventArgs>
        
        public EventHandler<VideoEventArgs> VideoEncoded;
        #endregion

        public void Encode(Video video, VideoFilter videoFilter)
        {
            Console.WriteLine("Encoding video ...");
            Thread.Sleep(3000);

            videoFilter(video);
            OnVideoEncoded(video);
        }

        protected virtual void OnVideoEncoded(Video video)
        {
            VideoEncoded?.Invoke(this, new VideoEventArgs
            {
                Video = video
            });
        }
    }
}