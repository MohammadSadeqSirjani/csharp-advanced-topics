﻿using System;

namespace EventsAndDelegates
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var video = new Video
            {
                Title = "John Wick - Episode 3"
            };

            var filter = new VideoFilter();
            var videoEncoder = new VideoEncoder(); //publisher
            var mailService = new MailService(); //subscriber
            var messageService = new MessageService(); //subscriber

            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded;
            videoEncoder.Encode(video, movie =>
            {
                filter.BlurredVideo(movie);
                filter.ChangeContrast(movie);
            });

            Console.WriteLine("Main process has been completed!");
        }
    }
}
