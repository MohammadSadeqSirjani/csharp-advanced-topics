﻿using System;

namespace EventsAndDelegates
{
    public class VideoFilter
    {
        public void ChangeContrast(Video video)
        {
            Console.WriteLine("Contrast the video ...");
        }

        public void BlurredVideo(Video video)
        {
            Console.WriteLine("Blurred the video ...");
        }
    }
}