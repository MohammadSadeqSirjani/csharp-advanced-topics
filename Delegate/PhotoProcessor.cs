﻿using System;

namespace Delegate
{
    public class PhotoProcessor
    {
        public delegate void PhotoFilterHandler(Photo photo);

        public void ProcessWithStaticMode(string path)
        {
            var photo = Photo.Load(path);

            var filters = new PhotoFilters();
            filters.ApplyBrightness(photo);
            filters.ApplyContrast(photo);
            filters.Resize(photo);

            photo.Save();
        }

        public void ProcessWithCustomDelegate(string path, PhotoFilterHandler filterHandler)
        {
            var photo = Photo.Load(path);

            filterHandler(photo);

            photo.Save();
        }

        public void ProcessWithAction(string path, Action<Photo> filterHandler)
        {
            //Two built-in dotnet framework delegates
            //System.Action<>
            //System.Func<>

            var photo = Photo.Load(path);

            filterHandler(photo);

            photo.Save();
        }
    }
}