﻿using System;

namespace Delegate
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var photoProcessor = new PhotoProcessor();
            var filter = new PhotoFilters();

            //photoProcessor.ProcessWithStaticMode("./etc/photo/apple.jpeg");

            //photoProcessor.ProcessWithCustomDelegate("./etc/photo/apple.jpeg", photo =>
            //{
            //    filter.ApplyBrightness(photo);
            //    filter.ApplyContrast(photo);
            //    filter.Resize(photo);
            //    RemoveRedEyesFilter(photo);
            //});

            photoProcessor.ProcessWithAction("./etc/photo/apple.jpeg", photo =>
            {
                filter.ApplyBrightness(photo);
                filter.ApplyContrast(photo);
                filter.Resize(photo);
                RemoveRedEyesFilter(photo);
            });

            Console.WriteLine("Main process has been completed!");
        }

        private static void RemoveRedEyesFilter(Photo photo)
        {
            Console.WriteLine("Apply RemovedRedEyes.");
        }
    }
}
