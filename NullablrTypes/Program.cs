﻿using System;

namespace NullableTypes
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            DateTime? yesterday = null;
            DateTime? date = yesterday ?? DateTime.Today;
            DateTime tomorrow = yesterday.GetValueOrDefault();
            Console.WriteLine($"HasValue: {date.HasValue}");
            Console.WriteLine($"GetValueOrDefault: {date.GetValueOrDefault()}");
            Console.WriteLine($"Value: {date.Value}");
        }
    }
}
