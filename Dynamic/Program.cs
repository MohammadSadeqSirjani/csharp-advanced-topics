﻿using System;
using System.ComponentModel;

namespace Dynamic
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            object obj = "Sadeq";
            //Console.WriteLine(obj.GetHashCode());

            //var methodInfo = obj.GetType().GetMethod("GetHashCode");
            //var detail = methodInfo?.Invoke(null, null);

            //dynamic excelObj = "Sadeq";
            //excelObj.Optimize();

            //dynamic name = "Sadeq";
            //name = 10;
            //name++;

            dynamic a = 10;
            dynamic b = 10;
            var c = a + b;
            var d = 10;
            var e = a + d;
        }
    }
}
